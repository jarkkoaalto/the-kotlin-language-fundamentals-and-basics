package section13

fun main(args: Array<String>){

    var person = Person()
    //person.name  ="Harry Potter"
    //person.age = 14

    with(person){
        name = "Harry Potter"
        age = 15
    }

    person.apply {
        name = "Harri Savenvalaja"
        age = 15
    }.startRun()

    println(person.name)
    println(person.age)
}

class Person{
    var name: String = ""
    var age: Int = -1

    fun startRun(){
        println("Now I an ready to run")
    }
}