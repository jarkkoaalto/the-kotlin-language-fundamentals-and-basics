package section13

fun main(args: Array<String>){
    val program1 = Program1()

    val myLambda:(Int,Int) -> Int = {x: Int, y:Int -> x+y}
    program1.addTwoNumbers(3,6, myLambda) // OR
    program1.addTwoNumbers(3,6,{ x, y -> x+y}) // 0R
    program1.addTwoNumbers(3,6){x,y->x+y} // OR
}

class Program1{
    fun addTwoNumbers(a: Int, b: Int, action:(Int, Int)-> Int){
        val result = action(a, b) // x +y = a + b = 3 + 6 = 9
        println(result)
    }
}