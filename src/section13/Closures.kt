package section13

fun main(args: Array<String>){
    val program2 = Program2()
    var result = 0

    //val myLambda:(Int,Int) -> Int = {x: Int, y:Int -> x+y}
    program2.addTwoNumbers(3,6){x,y -> result=x+y} // OR
    print(result)
}

class Program2{
    fun addTwoNumbers(a: Int, b: Int, action:(Int, Int)-> Unit){
        action(a, b) // x +y = a + b = 3 + 6 = 9 //   result = a + b = 3 + 6 = 9

    }
}