package section13

fun main(args: Array<String>) {
    val program = Program()
    program.addTwoNumbres(33, 33) // Simple way ...

    program.addTwoNumbres(45, 66, object :myInterface{
        override fun execute(sum: Int) {
            println(sum)
        }
    })

    val test: String = "Hi There"
    val myLambda: (Int) -> Unit = {s: Int -> println(s)} // Lambda expression [ function ]
    // program.addTwoNumbres(33,66, {s: Int -> println(s)})
}

class Program {

        fun addTwoNumbers(a:Int, b:Int,action:(Int) -> Unit){ // high level function with lambda as parameter
            val sum = a+b
            action(sum)
        }

        fun addTwoNumbres(a: Int, b: Int, action: myInterface) { // call also interface
            val sum = a + b
            action.execute(sum)


        }

        fun addTwoNumbres(a: Int, b: Int) { // Simple way ...
            val sum = a + b
            println(sum)
        }
    }


interface myInterface{
    fun execute(sum: Int)
}