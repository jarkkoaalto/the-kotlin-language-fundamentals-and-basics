package section13

fun main(args: Array<String>){
    val ohjelma = Ohjelma()
    ohjelma.reverseAndDisplay("hi there", {it.reversed()}) // Work if you have only one parameter
}

class Ohjelma{
    fun reverseAndDisplay(str: String, myFunc: (String) -> String){
        var result = myFunc(str) // return result
        print(result) // ereth ih
    }
}