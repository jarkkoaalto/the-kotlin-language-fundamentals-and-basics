package section14

fun main(args: Array<String>){

    var myMap = mapOf<Int, String>(1 to "Kotlin",2 to "Android",3 to "Apple")

    var myHasMap = HashMap<Int, String>()
    myHasMap.put(1,"Yogi")
    myHasMap.put(2, "Dino")

    for(element in myHasMap){
        println(element)
    }
    println()

    for(key in myMap.keys){
        println("Elementt at Key:$key = ${myMap[key]}")

    }
}