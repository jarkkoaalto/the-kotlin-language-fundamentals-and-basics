package section14

fun main(args: Array<String>){
    // "set" contains unique elements
    // "HashSet" also contaisns unique elements but Sequence is not guaranteed in output

    var mySet = setOf<Int>(3,53,67,1,89,54,3) // immutable, READ only
    var mySets = hashSetOf<Int>(2,54,3,88,64,21,7,6)
    mySets.remove(54)
    mySets.add(100)


    for (element in mySet){
        println(element)
    }
    println()
    for(element in mySets){
        println(element)
    }
}