package section14

// Array is Mutable but has fixed size
/*
    Immutable Collection -> immutable collection read only
    Mutable collection -> read and Write Both
 */

fun main(args: Array<String>){

    // Arrays
    var myArray = Array<Int>(5){0} // mutable. Fixed Size
    myArray[0] = 32;
    myArray[2] = 23;

    for (element in myArray){ // usung individual element object
       println(element)
    }

    for(index in 0..myArray.size - 1){
        println(myArray[index])
    }

}