package section14

/*

 */

fun main(args: Array<String>){

    var list = listOf<String>("Yogi","Superman","Spiderman") // iMMUTABLE, Fixed size, read only
    var lists = mutableListOf<String>("Banana","Kiwi","Apple")
    lists.add("Kotlin")


    for(element in list){
        print(element+" ")
    }
    println()
    for(element in 0..list.size -1){
        print(list[element]+" ")
    }

    println()
    for(element in lists){
        print(element + " ")
    }
}