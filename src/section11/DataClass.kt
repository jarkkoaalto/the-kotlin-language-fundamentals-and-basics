package section11

/*
Data class comapare only data not CLASS

CLASS WITHOUT DATA method
    section11.User@5e2de80c
    not Equal

CLASS WITH data method
    User(name=Sam, id=10)
    Equal

 */

fun main(args: Array<String>){
    var user1 = User("Sam", 10)
    var user2 = User("Sam",10)

    println(user1)

    if(user1 == user2){
        println("Equal")
    }else{
        println("not Equal")
    }

    var newUser = user1.copy(id=25)
    println(newUser)

}

data class User(var name: String, var id: Int){

}