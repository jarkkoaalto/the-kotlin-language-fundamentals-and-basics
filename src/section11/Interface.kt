package section11

fun main(args: Array<String>){

    var myButton  = MyButton()
    myButton.onClick()
    myButton.onTouch()

}

interface  MyInteerfacListener{ // you cannot create instance of interface
    var name: String  // properties in interface are abstract by default


    fun onTouch() // Working only that way

    public open fun onClick(){  // Normal methods are public and open by default NOT FINAL
        println("onClick Interface code: Button Clicked")
    }
}

interface  MySecondInteerfacListener{ // you cannot create instance of interface
    fun onTouch() {
        println("MySecondInterface: Ontouch")
    }

    public open fun onClick(){  // Normal methods are public and open by default NOT FINAL
        println("MySecondInterface: onClicked")
    }

}

class MyButton: MyInteerfacListener, MySecondInteerfacListener {
    // Body
    override var name : String = "Dummy_name"
    override fun onTouch() {
        super.onTouch()
        // Define your own code
        println("Button was touched")
    }

    override fun onClick() {
        super<MyInteerfacListener>.onClick()
        super<MySecondInteerfacListener>.onClick()
        println("Button was clicked")
    }
}
