package section11

fun main(args: Array<String>){


}

abstract class Person1{

    open var name : String = "dummy_name"

    fun goToSchool() {} // A normal function: Public and final by default
    open fun getHeight() {} // A "open" function ready to be overriden
    abstract fun eat() // A normal function: Public and final by default

    }
class State: Person1() { // Sub class or derived class

    override  var name: String = "dummy_state_name"


        override fun eat() {
    }

    override fun getHeight() { // Optional
        super.getHeight()
    }

}