package section11


class TestClass{
    fun testing() {

        var person = Person()
        print(person.c)
    }
}

open class Person{ // Super class
    private val a = 1
    protected  val b = 2
    internal val c = 3
    val d  = 10 // Public by default
}

class Indian: Person(){ // Derived Class or Sub class

        // a is not visible
        // b,c,d are visible



}

