package section09

fun main(args: Array<String>){
    var student = Student()
    println("Pass status: " + student.hasPassed(57))
    println("Sholarship status: " + student.isScholar(57))
}

//Act like inside Studentclass
fun Student.isScholar(marks:Int): Boolean{
    return marks > 95
}

class Student { // our own class
    fun hasPassed(marks: Int): Boolean{
        return marks > 40
    }
}