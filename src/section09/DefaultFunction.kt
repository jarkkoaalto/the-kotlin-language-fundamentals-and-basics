package section09

fun findVolume(lenght: Int, breadth: Int, height: Int = 10): Int {
    return lenght * breadth * height
}

fun main(args: Array<String>){

    println(findVolume(2,4)) // 3*4*10
    // Overrides the default value
    println(findVolume(3,4,50)) // 3*4*50
}