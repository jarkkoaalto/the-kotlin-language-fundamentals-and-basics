package section09

fun main(args: Array<String>){

/*
* 1. All infix functions are extension function
*   But all extension function are not Infix
*
* 2. Infix functions just have One parameter
 */

    val x:Int = 15
    val y:Int = 10

    // val greaterVal = x.greaterValue(y)
    val greaterVal = x.greaterValue(y)
    println(greaterVal)


// If function is two parameter Function don't be infix
infix fun Int.greaterValue(other: Int): Int {
    if(this < other){
        return this
    }else{
        return other
        }
    }
}