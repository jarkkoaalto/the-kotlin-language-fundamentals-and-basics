package section09

import java.math.BigInteger

tailrec fun getFibonacciNumber(n: Int, a: BigInteger, b: BigInteger): BigInteger{
    if (n == 0) {
        return b
    }else {
        return getFibonacciNumber(n - 1, a + b, a)
    }
}
// FIBONACCI
// 0 1 1 2 3 5 8 13 21 34

fun main(args: Array<String>){

    println(getFibonacciNumber( 10000, BigInteger("1"), BigInteger("0")))
}