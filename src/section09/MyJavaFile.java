package section09;

public class MyJavaFile {
    public static void main(String []args){
       int sum =  InteroperabilityKt.adding(6,9);
        System.out.println("Printing sum for Java File : " + sum);

        int result = getArea(2,3);

        System.out.println(result);
    }

    public static int getArea(int l, int b){
        return l*b;
    }
}
