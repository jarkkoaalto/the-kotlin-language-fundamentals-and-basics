package section09

fun main(args: Array<String>){
    var area = MyJavaFile.getArea(10, 30)
    println("Printing area from Kotlin file: " + area)
}

fun adding(a: Int, b:Int): Int {
    return a + b
}

/**
 *public Class Interoperability {
 *  public static void main(String [] args){
 *      }
 *
 *      public static int add(int a, int b){
 *      return a + b;
 *      }
 *  }
 */