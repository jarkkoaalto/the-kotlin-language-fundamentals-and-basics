package section04

/*
    This is main function. Entry point of the application
 */

fun main(args: Array<String>){

    var person = Person()
    person.name = "Jarkko"
    // person.display()
    println("The name of the person is  ${person.name}")
}


class Person {

    var name:String = ""
    // fun display(){
    /// println(name)

}