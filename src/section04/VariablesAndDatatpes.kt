package section04

/*
   Variables and datatypes
 */

fun main(args: Array<String>){
    var myNumber = 10
    var myDecimal = 1.0

    var myString: String // mutable string
    myString = "Hi there"
    myString = "Hello World"

    val myAnotherStr = "My Constants String Value" // Immutable string

    println (myDecimal)
    println(myString)
    println(myNumber)


}