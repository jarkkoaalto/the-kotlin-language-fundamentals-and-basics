package section08

fun main(args: Array<String>){
    // Loops in Kotlin
    // Continue Statement
    // Odd Numbers

    for(i in 1..10) {
        if (i%2 == 0) {
            continue
        }
        println(i)
    }


    outer@for(h in 1..3){
        for(x in 1..3){
            if(h == 2 && x == 2)
                continue@outer
            println("$h $x")
        }
    }
}