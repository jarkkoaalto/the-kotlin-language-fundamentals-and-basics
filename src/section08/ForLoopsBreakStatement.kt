package section08

fun main(args: Array<String>){
    // Loops in Kotlin
    // Break Statement

    // Lapelled For loop

   myLoop@ for(i in 1..3){
        for(h in 1..3){
            println("$i $h")
            if( i == 2 && h  == 2)
                break@myLoop
        }
    }
}