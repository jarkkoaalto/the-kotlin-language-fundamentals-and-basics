package section06

fun main(args: Array<String>){

    // WHEN as Expression
    val x = 21


    when(x){
        0,1-> println("x is 1 or 0")
        2-> println("x is 2")

        else -> {
            println("X value is unknown")
            println("I don't know what is x")

        }

    }

    var str: String = when(x){
        1 -> "x is 1"
        2 -> "x is 2"
        else -> {
            "x value is uknown"
            "z is an alien"
        }
    }

}