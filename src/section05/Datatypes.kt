package section05

fun main(srgs: Array<String>){
    var name = "Kevin"
    name  = "Alvin" // reasign
    val name1 = "Kevin" // Value immtable

    var age: Int = 10
    var isAlive : Boolean = true
    var marks: Float = 97.4F
    var percentage: Double = 90.65
    var gender: Char = 'M'

    println(isAlive)
    println(age)
    println(marks)
    println(percentage)
    println(gender)
    println(name)
    println(name1)
}