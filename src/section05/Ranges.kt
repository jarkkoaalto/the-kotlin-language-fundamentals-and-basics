package section05

fun main(args: Array<String>) {

    val r1 = 1..5
    // This range contains the number 1,2,3,4,5

    val r2 = 50 downTo 1
    // This range contains the number 5,4,3,2,1

    val r3 = 5 downTo 1 step 2
    // This range contains the number 5,3,1

    println(r1)
    println(r2)
    println(r3)

    var r4 = 'a'..'z'

    var isPresent = 'c' in r4
    println(isPresent)

    var countDown = 10.downTo(1)
    println(countDown)
}