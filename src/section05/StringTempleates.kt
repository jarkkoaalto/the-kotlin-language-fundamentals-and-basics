package section05

fun main(args: Array<String>){
    val name = "Sam"
    val str = "Hello $name"

    println("The satement is $str. The number in statement is ${str.length}")

    val a = 10
    val b = 5
    println("The sum of $a and $b is ${a+b}")


    var rect = Rectangle()
    rect.length = 5
    rect.breadth  = 3

    println("The lenght of rectangel is ${rect.length} and breadth is ${rect.breadth}")
}

class Rectangle {
    var length : Int = 0
    var breadth : Int = 0
}