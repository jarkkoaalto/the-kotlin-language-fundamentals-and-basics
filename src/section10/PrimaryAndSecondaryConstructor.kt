package section10

fun main(args: Array<String>){
    var dog = Doggi("Black","Pug")
}

open class Animal2{  // Super class
   /* init{
        println("From Animal init: $color")
    }*/
   var color: String  = ""
    constructor(color: String){
        this.color = color
        println("From Animal: $color")
    }
}
class Doggi: Animal2 {  //Deriver Class
    /* init{
        print("From Dog Init: $breed")
    }*/
    var breed: String = ""

    constructor(color: String, breed : String): super(color){
        this.breed = breed
        println("From Dog: $color and $breed")
    }
}

