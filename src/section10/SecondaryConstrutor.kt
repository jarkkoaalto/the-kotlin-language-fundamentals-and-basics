package section10



fun main(args: Array<String>){
    var students = Students("Syrink", 10)
    print(students.id)
}

class Students (var name: String) {
    var id: Int = -1
    init{
        println("Student has got name as $name and id is $id")
    }

    constructor(name: String, id:Int): this(name){
        // body of the secondary constructor is called after the init block
        this.id = id
    }
}