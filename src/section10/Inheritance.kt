package section10

fun main(args: Array<String>){
    var dog = Dog()
    dog.eat()
    println(dog.color)

    var cat = Cat()
    cat.eat()
    println(cat.color)

}

open class Animal {
    open var color : String = "White"
    open fun eat() {
        println("Animal Eating")
    }
}


class Dog :Animal() {
    var breed: String = ""
    override var color : String = "Brown"
    fun bark() {
        println("Bark")
    }

    override fun eat(){
        super.eat()
        println("Dog is eating")
    }
}

class Cat : Animal() {
    var age: Int = -1

    fun meaw(){
        println("Meow")
    }

    override fun eat(){
        super.eat()
        println("Cat is eating")
    }
}