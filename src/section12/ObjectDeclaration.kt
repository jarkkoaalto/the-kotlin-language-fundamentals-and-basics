package section12

/*
 in kotlin we cannot declare "Static" varables like Java

object Customer{
    var id: Int = -1 // Behaving as Static variable

    fun registerCustomer(){ // Behaving as Static METHOD
        // your code
    }
}
*/

fun main(args: Array<String>){

    CustomerData.count = 99
    CustomerData.typeOfCustomers()
    println(CustomerData.count)

    CustomerData.count = 901
    println(CustomerData.count)

    CustomerData.myMethod("Hello")
}

open class MySuperClass{
    open fun myMethod(str: String){
        println("MySuperClass")
    }
}

object CustomerData: MySuperClass(){
    var count : Int = -1    // behaves like Static variable

    fun typeOfCustomers(): String{ // behaves like static method
        return "Human"
    }

    override fun myMethod(str: String){
        super.myMethod(str)
        println("Ogject CustomerData: "+ str)
    }

    init {
        // You can create code here ...
    }

}