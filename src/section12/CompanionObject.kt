package section12


fun main(args: Array<String>){

    MyClass.count
    MyClass.typeOfCustomers()

}

 class MyClass{
   companion object  {
       var count: Int = -1    // behaves like Static variable

        // @JvmStatic
       fun typeOfCustomers(): String { // behaves like static method
           return "Human"
       }
    }
}