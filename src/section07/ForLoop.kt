package section07

fun main(args: Array<String>){

    // Loops in Kotlin
    // For
    for(i in 1..10){
        println(i)
    }

    // Even numbers
    for(j in 1..10){
        if( j %2 == 0){
            println(j)
        }
    }

    for(x in 1..3){
        println("Hi there")
    }
}