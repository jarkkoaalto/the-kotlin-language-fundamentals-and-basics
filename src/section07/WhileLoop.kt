package section07

fun main(args: Array<String>){

    // Loops in Kotlin
    // While loop

    var i:Int = 1
    while (i <= 10){
        println(i)
        i++
    }
}
